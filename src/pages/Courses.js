// Identify which components will be displayed
import { useState, useEffect }from 'react'
import Hero from './../components/Banner';
import CourseCard from './../components/CourseCard'
import {Container} from 'react-bootstrap'
const data = {
	title: 'Course Catalog',
	content: 'Browse through our Catalog of courses.'
};

// Catalog all active courses
	// 1. Container for each course that we will retrieve from the database
	// 2. Declare a state for the courses collection by default it is an empty array
	// 3. Create a side effect that will send a request to our backend api
	// 4. Pass down the retrieve resources inside the component as props

export default function Courses () {

	// storage for all active courses
	// the effect that we are going to create is the fetching of data from the server
	const [coursesCollection, setCourseCollection] = useState([]);

	useEffect(() => {
		// fetch() => is a JS method which allows us to pass/create request from api
		// SYNTAX : fetch (<request URL>, Options)
		// GET => no need to insert an options parameter
		// remember that once you send a request to an endpoint, a promise is return as a result (fulfilled, rehjected, pending)
		// we need to be able to handle the outcome of the promise
		// we need to make the response usable on the frontend side
		// upon converting a fetch data to a json format, a new promise will be executed
		fetch('https://stark-scrubland-25171.herokuapp.com/courses/')
		.then(res => res.json())
		.then(convertedData => {
			// console.log(convertedData);
			// we want to resource to be displayed in the page
			// since the data is stored in an array, we will iterate the array to explicitly get each document one by one.
			// there will be multiple course card component that will be rendered that corresponds to each docyment retrieved from the database. we need to utilize a storage to contain the cards that will be rendered
			// key attribute as reference to avoid rendering duplicates of the same/element/object.
			setCourseCollection(convertedData.map(course => {
				return (
					<CourseCard key={course._id} courseProp={course}/>
				)
			})) 

		})

	},[]);

	return (
		<>
			<Hero bannerData={data}/>
			<Container>
				{coursesCollection}
			</Container>
		</>
	);
};