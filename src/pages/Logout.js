// identify the components that will be used for this page
// import Hero from './../components/Banner'
import { useContext, useEffect } from 'react';

// page navigation to redirect the user back to login/home
import { Navigate } from 'react-router-dom'

// the module would need to consume the state of the user provided by the app.js module
import UserContext from '../UserContext';

// create a function that will describle the procedures upon logging out and unmounting the user from the app.



// const data = {
// 	title: 'Logout Page',
// 	content: 'You have logged out. See you next time!'
// };
// create a function that will describe the procedures upon logging out and unmounting the user from the app.
// keep in mind the logout has NO physical entity => the browser page will not be able to render any output on the display

export default function Logout () {

	// the important purpose of this module is to unmount the user from the application
	// let's destructure our context object
	const { setUser, unsetUser } = useContext(UserContext)

	unsetUser();

	// create a side effect, this effect that we will create will update the state of the global user.

	// this is to make sure that the changes about the user will automatically/promptly be recognized in a global state.

	useEffect(() => {
		// update the global state of the user.
		setUser({
			id: null,
			isAdmin: null
		})
	},[setUser]);



	return (
		< Navigate to ="/login" replace={true} />
	);
};