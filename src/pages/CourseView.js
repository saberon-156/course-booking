// This will serve as a page whenever the client would want to display or select a single course or item from the catalog

import Hero from './../components/Banner'
// Grid System
import {Row, Col, Card, Button, Container} from 'react-bootstrap'

// declare a state for the course details. we need to use the correct hook.
import { useState, useEffect }from 'react'

// import sweetalert
import Swal from 'sweetalert2';

// routing component
import { Link, useParams } from 'react-router-dom'

const data = {
	title: 'Welcome to B156 Booking-App!',
	content: 'We offer diverse courses that will help you jumpstart your future career!'
};

export default function CourseView () {

	// state of our course details
	const [courseInfo, setCourseInfo] = useState({
		name: null,
		description: null,
		price: null,
	});

	// the useParams will return an obbject that will contain path variables stored in the url
	console.log(useParams());
	// observe, it should contain an object that stores the id of the targeted course
	const {id} =  useParams()
	console.log(id);

	// create a side effect which will send a request to our backend API for course-booking
	useEffect(() => {
		// send request to our API to retrieve info about the course
		// syntax: fetch
		fetch(`https://stark-scrubland-25171.herokuapp.com/courses/${id}`).then(res => res.json()).then(convertedData => {
			// console.log(convertedData)
			setCourseInfo({
				name: convertedData.name,
				description: convertedData.description,
				price: convertedData.price
			})
		});
	},[id])


	const enroll =  () => {
		return (
			Swal.fire ({
				icon: "success",
				title: 'Enrolled Successfully!',
				text: 'Thank you for enrolling to this course'
				})
		);
	};

	return (
		<>
			<Hero bannerData={data}/>
			<Row>
				<Col>
					<Container>
						<Card className="text-center">
							<Card.Body>
								{/*Course Name*/}
								<Card.Title>
									<h3>{courseInfo.name}</h3>
								</Card.Title>
								{/*Course Description*/}
								<Card.Subtitle>
									<h6 className="my-4">Description:</h6>
								</Card.Subtitle>
								<Card.Text>
									{courseInfo.description}
								</Card.Text>
								{/*Course Price*/}
								<Card.Subtitle>
									<h6 className="my-4">Price: </h6>
								</Card.Subtitle>
								<Card.Text>
									PHP: {courseInfo.price}
								</Card.Text>
							</Card.Body>
							<Button variant="warning" className="btn-block" onClick={enroll}>
								Enroll
							</Button>

							<Link className="btn btn-success btn-block mb-4" to="/login">
								Login to Enroll.
							</Link>
						</Card>
					</Container>
				</Col>
			</Row>
		</>
	);
};