// [Activity]
// [MAIN GOAL] Create Add Course Page

// 1.Acquire all needed components
// 2. Identify all info needed
// 3. Expose data to the entry point and assign a designated end point
// 4. Add navbar
// 5. Create a simulation
// 6. Submit repo in boodle
 
// identify the components that will be used for this page
import Hero from './../components/Banner';
import {Form, Button, Container} from 'react-bootstrap'

// for state
import { useState } from 'react';

// import sweetalert
import Swal from 'sweetalert2';

const data = {
	title: 'Course Creation Page',
	content: 'Enter course details here.'
};

export default function CreateCourse () {

	const [courseName, setCourseName] = useState('')	
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')

	const CreateCourse = (event) => {
		event.preventDefault();
		console.log(courseName);
		console.log(description);
		console.log(price);
		return (
			Swal.fire ({
				icon: "success",
				title: 'Course created successfully!',
				text: 'Course has been added to the list.'
			})
		);
	};
	return (
		<>
			<Hero bannerData={data}/>
			<Container>
				<h1 className="text-center">Add Course</h1>
				<Form onSubmit={e => CreateCourse (e)}>
				
					{/*Course Name*/}
					<Form.Group>
						<Form.Label>Course Name:</Form.Label>
						<Form.Control type="text" placeholder="Add Course name" required value={courseName} onChange={event => {setCourseName(event.target.value)}} />
					</Form.Group>

					{/*Course Description*/}
					<Form.Group>
						<Form.Label>Course Description:</Form.Label>
						<Form.Control type="text" placeholder="Add Course Description" required value={description} onChange={event => {setDescription(event.target.value)}}/>
					</Form.Group>

					{/*Price*/}
					<Form.Group>
						<Form.Label>Price:</Form.Label>
						<Form.Control type="number" placeholder="Course Price" required value={price} onChange={event => {setPrice(event.target.value)}}/>
					</Form.Group>

					{/*Submit button*/}
					<Button type="Submit" variant="success" className="btn-block mb-5">Create Course</Button>	
				</Form>
			</Container>
			
		</>
	);
};