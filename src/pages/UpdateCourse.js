import Hero from './../components/Banner';
import {Form, Button, Container} from 'react-bootstrap'

// import sweetalert
import Swal from 'sweetalert2';

const data = {
	title: 'Course Update Page',
	content: 'Enter new course details here.'
};

export default function UpdateCourse () {

	const UpdateCourse = (event) => {
		event.preventDefault();
		return (
			Swal.fire ({
				icon: "success",
				title: 'Course updated successfully!',
				text: 'Course has been updated.'
			})
		);
	};
	return (
		<>
			<Hero bannerData={data}/>
			<Container>
				<h1 className="text-center">Update Course</h1>
				<Form onSubmit={e => UpdateCourse (e)}>
				
					{/*Course Name*/}
					<Form.Group>
						<Form.Label>Course Name:</Form.Label>
						<Form.Control type="text" placeholder="Add Course name" required />
					</Form.Group>

					{/*Course Description*/}
					<Form.Group>
						<Form.Label>Course Description:</Form.Label>
						<Form.Control type="text" placeholder="Add Course Description" required/>
					</Form.Group>

					{/*Price*/}
					<Form.Group>
						<Form.Label>Price:</Form.Label>
						<Form.Control type="number" placeholder="Course Price" required/>
					</Form.Group>

					{/*Disable/Enable Button*/}	
					<Form.Check 
    				type="switch"
   					 id="custom-switch"
    				label="Active"
  					/>

					{/*Submit button*/}
					<Button type="Submit" variant="success" className="btn-block mb-5">Update Course</Button>

					

				</Form>
			</Container>
			
		</>
	);
};