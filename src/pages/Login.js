// for state
import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

// identify the components that will be used for this page
import Hero from './../components/Banner';
import {Form, Button, Container} from 'react-bootstrap'

// import sweetalert
import Swal from 'sweetalert2';


// we will declare states for our form components for us to be able to access and manage the values in each of the form elements

const data = {
	title: 'Login Page',
	content: 'Login to your account.'
};

export default function Login () {

	const { user, setUser } = useContext(UserContext)

	// declare an initial/default state for our form elements
	// bind/lock the form elements to the desired state
	// assign the states to their respective components
	// SYNTAX const/let [getter, setter] = useState()

	const [email, setEmail] = useState('');	
	const [password, setPassword] = useState('');

	// what validation are we going to run on the email.
	// (format: @, dns)
	// search a charcter within a string
	let addressSign = email.search('@');
	// if the search() finds no match inside the string it will return -1
	// if you are going to pass down multiple values on the query, contain it inside an array
	let dns = email.search('.com')

	// state login button
	const [isActive, setIsActive] = useState(false);
	// apply a conditional rendering to the button component

	// email
	const [isValid, setIsValid] = useState(false);
	// create a side effect that will make our page reactive
	useEffect(() => {
		if (dns !== -1 && addressSign !== -1) {
			setIsValid(true)
			if (password !== '') {
				setIsActive(true);
			} else {
				setIsActive(false);
			}
		} else {
			setIsValid(false)
		}
	},[email, password, addressSign, dns])

	// simulation
	const loginUser = async (event) => {
		event.preventDefault();

		// send a request if a user's identity is tru
		fetch(`https://stark-scrubland-25171.herokuapp.com/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type':"application/json"
			},
			body: JSON.stringify({
   				email: email,
    			password:password
			})
		}).then(res => res.json())
		.then(dataJ => {
			let token = dataJ.access;
			// console.log(token);//token

			if (typeof token !== 'undefined') {
				
				localStorage.setItem('accessToken', token);

				fetch('https://stark-scrubland-25171.herokuapp.com/users/details', {
				   headers: {
				      Authorization: `Bearer ${token}`
				   }
				})
				.then(res => res.json())
				.then(convertedData => {
				   if (typeof convertedData._id !== "undefined") {
				    setUser({
				       id: convertedData._id,
				       isAdmin: convertedData.isAdmin
				    });
				    Swal.fire ({
				    	icon: "success",
				    	title: 'Logged in successfully!',
				    	text: 'Thank you for logging in.'
				    })
				   } else {
				    setUser({
				       id: null,
				       isAdmin: null
				    })  
				   }
				}); 
			} else {
				Swal.fire ({
					icon: 'error',
					title: 'Logged failed!',
					text: 'Please make sure credentials are accurate.'
				})
			}
		})
		
	};

	return (
		user.id ?
		<Navigate to="/courses" replace={true}/>
		:
		<>
			<Hero bannerData={data}/>
			<Container>

				<h1 className="text-center">Login Form</h1>
				<Form onSubmit={e => loginUser (e)}>
					{/*Email Address*/}
					<Form.Group>
						<Form.Label>Email</Form.Label>
						<Form.Control type="email" placeholder="Enter your email address" required value={email} onChange={event => {setEmail(event.target.value)}}/>
						{
							isValid ?
							<h6 className="text-success">Email is valid!</h6>
							: <h6 className="text-mute">Email is invalid.</h6>
						}
						
					</Form.Group>

					{/*Password*/}
					<Form.Group>
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" placeholder="Enter your password" required value={password} onChange={event => {setPassword(event.target.value)}}/>
					</Form.Group>
					{/*Submit button*/}
					{
						isActive ?
						<Button type="Submit" variant="success" className="btn-block mb-3"> Login</Button>	
						: 
						<Button type="Submit" variant="secondary" className="btn-block mb-3" disabled> Login</Button>

					}
				
				</Form>
			</Container>
			
		</>
	);
};