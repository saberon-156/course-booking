// identify needed components
import { Card } from 'react-bootstrap';

// element with routing capability
import { Link } from 'react-router-dom';
// we are going to use this link component

export default function CourseCard({courseProp}) {
	return (
					<Card className="course-card m-4 d-md-inline-flex d-sm-inline-flex">
						<Card.Body>
							<Card.Title>
								{courseProp.name}
							</Card.Title>
							{/*<Card.Img variant="top" src={`${courseProp._id}.jpg`}/>*/}
							<Card.Text>
								{courseProp.description}
							</Card.Text>
							<Card.Text>
								PHP {courseProp.price}
							</Card.Text>
							<Link to={`view/${courseProp._id}`} className="btn btn-success">
								View Course
							</Link>
						</Card.Body>
					</Card>
				
	)
}