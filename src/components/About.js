// [ACTIVITY]
// [TASK]
// Create a responsive component that will serve as an about page for the project author

// Components
// Title: About me
// Full Name: 'Author Name'
// Job Description: Full Stack Web Developer
// Description: 'About Yourself'
// Contacts:
// 	(Email, Mobile No, Address)

import {Row, Col} from 'react-bootstrap'

export default function About () {
	return (
		
			<Row>
				<Col className="bg bg-warning">
					<h1 className="pb-3 mt-4 ml-4">About Me</h1>
					<img src="ninakler.JPG" className="w-25 ml-4" alt="None"/>
					<h2 className="ml-4 text-success">Nina Saberon</h2>
					<h4 className="ml-4 text-muted"><em>Full Stack Web Developer</em></h4>
					<p className="ml-4">I am a Full Stack Web Developer, a Registered Psychometrician, and a Musician.</p>
					<h4 className="ml-4">Contact Me</h4>
					<ul className="ml-3">
						<li>Email: keikureru@gmail.com</li>
						<li>Mobile No: 09876547896</li>
						<li>Address: Cebu, City Cebu 6000</li>
					</ul>
				</Col>
			</Row>
		
	);
};