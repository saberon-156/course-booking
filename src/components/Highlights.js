// Grid System and Card
import {Row, Col, Card, Container} from 'react-bootstrap'

// format the card with the help of utility classes of bootstrap

export default function Highlights () {
	return(
		<Container>
			<Row className="my-3">
				{/*First Highlight*/}
				<Col xs={12} md={4}>
					<Card className="p-4 cardHighlight">
						<Card.Body>
							<Card.Title> Learn from home!</Card.Title>
							<Card.Text>
								Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, quo.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				{/*Second Higlight*/}
				<Col xs={12} md={4}>
					<Card className="p-4 cardHighlight">
						<Card.Body>
							<Card.Title> Study Now, Pay Later!</Card.Title>
							<Card.Text>
								Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, quo.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				{/*Third Highlight*/}
				<Col xs={12} md={4}>
					<Card className="p-4 cardHighlight">
						<Card.Body>
							<Card.Title>Be Part of our Community!</Card.Title>
							<Card.Text>
								Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, quo.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		
	);
};